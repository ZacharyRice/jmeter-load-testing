# jmeter-load-testing



## Getting started

Prerequisites:

Java: https://www.java.com/en/download/
Download the latest version of jMeter: https://jmeter.apache.org/download_jmeter.cgi

## Running load Tests via the CLI

- [ ] Download the latest repo
- [ ] Execute the tests via the command line without a UI. Example:

```
cd C:\Code\apache-jmeter\apache-jmeter-5.6.3\bin
jmeter -n -t C:\Code\jmeter-load-testing\LoadTesting-ARSFinancial.com.jmx -l C:\Code\jmeter-load-testing\results\data.csv

```
Command line switches:
- -n No GUI
- -t Path to .jmx Test file from the gitlab repo
- -l path to CSV report
